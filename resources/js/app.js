
import Vue from 'vue'

import VueAxios from 'vue-axios';
import axios from 'axios';
import VueRouter from 'vue-router';

Vue.use(VueAxios, axios)
Vue.use(VueRouter);

import App from './view/App';
import dashboard from './components/dashboard';
import Login from './components/login';

import CreateComponent from './components/CreateComponent';
import EditComponent from './components/EditComponent';
import ViewComponent from './components/ViewComponent';

import createDarasa from './components/darasas/createDarasa';
import editDarasa from './components/darasas/editDarasa';
import viewDarasa from './components/darasas/viewDarasa';

import createDorm from './components/dorms/createDorm';
import editDorm from './components/dorms/editdorm';
import viewDorm from './components/dorms/viewdorm';

window.Vue = require('vue');

const router = new VueRouter({
    mode: 'history',
    routes: [

        {
            name: "dashboard",
            path: "/dashboard",
            component: dashboard
        },
        {
            name: "login",
            path: "/",
            component: Login
        },
        {
            name: "create",
            path: "/create",
            component: CreateComponent
        },
        {
            name: "edit",
            path: "/edit/:id",
            component: EditComponent
        },
        {
            name: "view",
            path: "/view",
            component: ViewComponent
        },
        {
            name: "createdarasa",
            path: "/createdarasa",
            component:createDarasa 
        },
        {
            name: "editdarasa",
            path: "/editdarasa",
            component: editDarasa
        },
        {
            name: "viewdarasa",
            path: "/viewdarasas",
            component: viewDarasa
        },
        {
            name: "createdorm",
            path: "/createdorm",
            component: createDorm
        },
        {
            name: "editdorm",
            path: "/editdorm/:id",
            component: editDorm
        },
        {
            name: "viewdorms",
            path: "/viewdorms",
            component: viewDorm
        }
        
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    watch: {
        '$route'(to, from) {
            if (this.$route.path === '/') {
               
            } else {
                const token = 'Bearer  ' + localStorage.getItem('token');
                let uri = 'api/auth/user';
                axios.get(uri, { headers: { Authorization: token } }).then(response => {
                    $('#navbarDropdownMenuLink').text(response.data.name);
                }).catch(error => {
                    localStorage.removeItem('token');
                    this.$router.push({ name: 'login' });
                    alert('Please login')
                });
           }
        }
    }
});

