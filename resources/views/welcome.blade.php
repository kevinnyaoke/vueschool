<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>vueschool login</title>
    <link rel="stylesheet" href="ui/css/bootstrap.min.css">
<script src="ui/js/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="ui/js/bootstrap.min.js"></script>
<script src="{{asset('alert/alertify.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('alert/css/alertify.min.css')}}">
    <link rel="stylesheet" href="{{asset('alert/css/themes/default.min.css')}}">
</head>
<body>
    <div id="app">
        <app></app>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>