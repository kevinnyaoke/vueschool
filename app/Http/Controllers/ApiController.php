<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;

class ApiController extends Controller
{
    public function getAllStudents(){

        $students=Students::all();
        return ['status'=>true,'list'=>$students] ;
    }

    public function createStudent(Request $request){
     $data=Students::create($request->all());
     return ['status'=>true,'message'=>'Student added successfully'];
    }

    public function getStudent($id){
      $student = Students::find($id);
      return ['status'=>true,'student'=>$student];
  
    }

    public function updateStudent(Request $request, $id){

      $student = Students::find($id);

      $student->update($request->all());

     return['status'=>true,'message'=>'Successfully updated'];
    }


    public function deleteStudent($id){

      $student = Students::find($id);

      $student->delete();

      return ['status'=>true,'message'=>'Successfully deleted'];
        
    }

}
