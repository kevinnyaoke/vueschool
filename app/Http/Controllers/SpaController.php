<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Darasas;
use App\Dorms;

class SpaController extends Controller
{
    public function index(){
        return view('welcome');
    }

//darasa functions
        public function getAllDarasas(){

        $darasas=Darasas::all();
        return ['status'=>true,'list'=>$darasas] ;
    }

    public function createDarasa(Request $request){
     $data=Darasas::create($request->all());
     return ['status'=>true,'message'=>'Class created successfully'];
    }

    public function getDarasa($id){
      $darasa = Darasas::find($id);
      return ['status'=>true,'darasa'=>$darasa];
    }


    public function updateDarasa(Request $request, $id){
      $darasa = Darasas::find($id);
      $darasa->update($request->all());
     return['status'=>true,'message'=>'Successfully updated'];
    }


    public function deleteDarasa($id){

      $darasa = Darasas::find($id);

      $darasa->delete();

      return['status'=>true, 'message'=>'Class deleted'];
        
    }

    //dorms functions

       public function getAllDorms(){

        $dorms=Dorms::all();
        return ['status'=>true,'list'=>$dorms] ;
    }

    public function createDorm(Request $request){
     $data=Dorms::create($request->all());
     return ['status'=>true,'message'=>'Dorm added successfully'];
    }

    public function getDorm($id){

      $dorm = Dorms::find($id);
      return ['status'=>true,'dorm'=>$dorm];
    }


    public function updateDorm(Request $request, $id){

      $dorm = Dorms::find($id);

      $dorm->update($request->all());

     return['status'=>true,'message'=>'Successfully updated'];
    }


    public function deleteDorm($id){

      $dorm = Dorms::find($id);

      $dorm->delete();

      return['status'=>true, 'message'=>'Class deleted'];
        
    }
  
}
