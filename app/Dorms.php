<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dorms extends Model
{
    protected $fillable=['name','code'];
}
