<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Darasas extends Model
{
    protected $fillable=['name','code','fee'];
}
