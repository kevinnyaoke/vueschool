<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });


});

Route::post('/students','ApiController@createStudent');
Route::get('/students','ApiController@getAllStudents');
Route::get('/students/edit/{id}','ApiController@getStudent');
Route::put('/students/update/{id}','ApiController@updateStudent');
Route::delete('/students/delete/{id}','ApiController@deleteStudent');

Route::post('/darasas','SpaController@createDarasa');
Route::get('/darasas','SpaController@getAllDarasas');
Route::get('/darasas/edit/{id}','SpaController@getDarasa');
Route::put('/darasas/update/{id}','SpaController@updateDarasa');
Route::delete('/darasas/delete/{id}','SpaController@deleteDarasa');

Route::post('/dorms','SpaController@createDorm');
Route::get('/dorms','SpaController@getAllDorms');
Route::get('/dorms/edit/{id}','SpaController@getDorm');
Route::put('/dorms/update/{id}','SpaController@updateDorm');
Route::delete('/dorms/delete/{id}','SpaController@deleteDorm');